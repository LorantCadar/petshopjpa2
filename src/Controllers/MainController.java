package Controllers;

import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.Main;
import model.Animal;
import model.Programari;
import util.DatabaseUtil;

// bitbucket vlad12
public class MainController implements Initializable {

	
	@FXML
	private ListView<String> listView;	// ID in FXML
	private Pane doctorview; // not used, found another way
	@FXML
	private Text phName, phAge, phWeight, phOwner, phPhone, phEmail, tipProgramare; // Placeholder Text where data of the animal goes. Formula PH -Placeholder + Name,age,weight etc. 
	
	
	
	
	
	public void populateMainListView() 
	{
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUp();
		dbUtil.startTransaction();
		List<Animal> animalDBList = (List<Animal>) dbUtil.animalList(); 
		List<Programari> progrDBList = (List<Programari>) dbUtil.programriList();
		ObservableList<String> animalNamesList = getAnimalName(animalDBList);
		ObservableList<String> animalPlusHour = getAnimalNameAndAppointment(animalDBList, progrDBList);
		
		listView.setItems(animalNamesList);
		listView.refresh();
		dbUtil.closeEntityManager();
	}
	@FXML
	void onButtonClick() // When animal is selected from list, the appointment view for the doctor shows. Working! 
	{
		int index = listView.getSelectionModel().getSelectedIndex();
		DatabaseUtil db = new DatabaseUtil(); 
		db.setUp();
		db.startTransaction();
		List<Animal> animalDBList = (List<Animal>) db.animalList(); 		
		
		for(Animal a: animalDBList)
		{
			if((index+1) == a.getIdAnimal())  // My DB index starts at 1.
			{
				System.out.print("Animal ID is " + a.getIdAnimal()); // FOR DEBUG. 
				System.out.print("Animal name is " + a.getName());
				phName.setText(a.getName());
				phAge.setText(a.getAge());
				phWeight.setText(a.getWeight());
				phOwner.setText(a.getOwner());
				phPhone.setText(a.getPhone());
				phEmail.setText(a.getEmail());			
			}		
		}		
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1){
		populateMainListView();
		
	}
	
	// EVERYTHING BELOW IS EXTRA AND NOT RUNNING/CALLED. JUST TRYING OUT DIFFERENT IMPLEMENTATIONS. NOT NEEDED FOR THE SOFTWARE TO WORK CORRECTLY. 
	
	public ObservableList<String> getAnimalNameAndAppointment(List<Animal> animals, List<Programari> progr) // Not used
	{
		ObservableList<String> namePlusHour = FXCollections.observableArrayList();
		
		for(Animal a: animals)
		{
			for(Programari p: progr)
			{
				namePlusHour.add(a.getName() + " " + p.getHour());
			}
			
		}
		
		return namePlusHour;
	}
	
	public ObservableList<String> getAnimalAge(List<Animal> animals)
	{
		ObservableList<String> age = FXCollections.observableArrayList();
		for(Animal a: animals)
		{
			age.add(a.getAge());
		}
		
		return age;

	
	}
	
	public ObservableList<String> getAnimalName(List<Animal> animals)
	{
		
		ObservableList<String> names = FXCollections.observableArrayList();
		Programari p = new Programari();
		
		
		for(Animal a: animals)
		{
			names.add(a.getName()); // + " " + p.getHour()); 
			
		}
		return names;
	}
	public void populateProramariView() // Not used
	{
		
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUp();
		dbUtil.startTransaction();
		List<Programari> progrDBList = (List<Programari>) dbUtil.programriList();
		ObservableList<String> programariList = getAnimalProgramari(progrDBList);
		
		listView.setItems(programariList);
		listView.refresh();
		dbUtil.closeEntityManager();
	}
	
	public ObservableList<String> getAnimalProgramari(List<Programari> programari) // not used
	{
		ObservableList<String> program = FXCollections.observableArrayList();
		for(Programari p: programari) {
			program.add(p.getHour());
		}
		return program;
	}
	
	@FXML
	void insertAnimalButtonClicked(ActionEvent e) // Don't use! 
	{
		
		DatabaseUtil db = new DatabaseUtil(); 
		db.setUp();
		db.startTransaction();
		Animal a = new Animal(); 
		a.setName("Animal Test");
		Random i = new Random();
		a.setIdAnimal(i.nextInt());
		a.setAge("12");
		db.saveAnimal(a);
		db.commitTransaction();
		db.closeEntityManager();
	
		
	}
	
	
	
	@FXML
	
	void populateAppointment(int AnimalIndex)
	{
		
	}
	
	@FXML
	void addAppointmentClicked(ActionEvent e)
	{
		//BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("NewWindow.fxml"));
		//Scene scene = new Scene(root, 800,800); 
	
		//Main.primaryStage.setScene(scene);
		//scene.show();
	}

}
