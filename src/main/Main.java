package main;



import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.Medic;
import model.Programari;
import util.DatabaseUtil;

public class Main extends Application{
	
	public static Stage primaryStage;
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			this.primaryStage = primaryStage;
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/Controllers/MainView.fxml"));
			Scene scene = new Scene(root, 800,800); 
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {
		
		launch(args);
		
	}
	
}


/* PENTRU JPA 
 * public static void main(String[] args) throws Exception
	{
		DatabaseUtil dbUtil = new DatabaseUtil(); 
		Animal bob = new Animal(); 
		Medic medic = new Medic();
		Programari programari = new Programari();
		
		List<Programari> bobProgramari = new ArrayList<>();
		
		medic.setIdmedic(1);
		medic.setNume("JonnyBravo");
		programari.setIdprogramari(1);
		programari.setTip("Operatie");
		bobProgramari.add(programari);
		bob.setIdAnimal(3);
		bob.setName("Yolo");
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(bob);
		dbUtil.saveMedic(medic);
		dbUtil.saveProgramare(programari);
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.closeEntityManager();
		
	}
	*/
