package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Medic;
import model.Programari;

public class DatabaseUtil {
	
	public static EntityManagerFactory entityManagerFactory; 
	public static EntityManager entityManager; 
	
	public void setUp() {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShopJpa");
		entityManager = entityManagerFactory.createEntityManager();
	
	}
	
	public void saveAnimal(Animal animal)  // Se salveaza obiectul Animal
	{
		entityManager.persist(animal);
	}
	public void saveMedic(Medic medic)
	{
		entityManager.persist(medic);
	}
	public void saveProgramare(Programari programari)
	{
		entityManager.persist(programari);
	}
	
	public void startTransaction() // Initializare comunicare cu server
	{
		entityManager.getTransaction().begin(); 
	}
	public void commitTransaction() // transmitem ce am salvat la server
	{
		entityManager.getTransaction().commit();
	}
	public void closeEntityManager() // inchidem conexiunea
	{
		entityManager.close();
	}
	public List<Animal> animalList()  // Toate animalele din baza de date.
	{
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a", Animal.class).getResultList();
		return animalList;
	}
	
	public void printAllAnimalsFromDB()
	{
		List<Animal> resultsAnimal = entityManager.createNativeQuery("Select * from petshop.animal", Animal.class).getResultList();
		List<Medic> resultsMedic = entityManager.createNativeQuery("Select * from petshop.medic", Medic.class).getResultList();
		List<Programari> resultsProgramari = entityManager.createNativeQuery("Select * from petshop.programari", Programari.class).getResultList();
		
		for(Animal animal : resultsAnimal)
		{
			System.out.println("Animal: " + animal.getName() + " has ID: " + animal.getIdAnimal());
		}
		for(Medic medic : resultsMedic)
		{
			System.out.println("Medic: " + medic.getNume() + " has ID: " + medic.getIdmedic());
		}
		for(Programari programari : resultsProgramari)
		{
			System.out.println("Animal: " + programari.getTip() + " has ID: " + programari.getIdprogramari());
		}
		
	}

	public List<Programari> programriList() {
		
		List<Programari> programari = (List<Programari>)entityManager.createQuery("SELECT a FROM Programari a", Programari.class).getResultList();
		return programari;
	}
}
