package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the programari database table.
 * 
 */
@Entity
@NamedQuery(name="Programari.findAll", query="SELECT p FROM Programari p")
public class Programari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idprogramari;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	private Time hour;

	private int idanimal;

	private int idmedic;

	private String tip;

	public Programari() {
	}

	public int getIdprogramari() {
		return this.idprogramari;
	}

	public void setIdprogramari(int idprogramari) {
		this.idprogramari = idprogramari;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getHour() {
		return this.hour.toString();
	}

	public void setHour(Time hour) {
		this.hour = hour;
	}

	public int getIdanimal() {
		return this.idanimal;
	}

	public void setIdanimal(int idanimal) {
		this.idanimal = idanimal;
	}

	public int getIdmedic() {
		return this.idmedic;
	}

	public void setIdmedic(int idmedic) {
		this.idmedic = idmedic;
	}

	public String getTip() {
		return this.tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

}